﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GerenciamentoDeAluno.Models;
using System.Web.Security;

namespace GerenciamentoDeAluno.Controllers
{
    public class UsuarioController : Controller
    {
        private GerenciamentoContexto db = new GerenciamentoContexto();

        
        // GET: Usuario
        [Authorize]
        public ActionResult Index()
        {
            var usuarios = db.Usuarios.Include(u => u.Cidade);
            ViewBag.PaginaAtual = 1;
            return View(usuarios.ToList());
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index([Bind(Include = "UsuarioId,Nome,Cpf,Sexo,Fone,DataCadastro,CidadeId,Email,Senha")] Usuario usuario)
        {
            var result = db.Usuarios.Where(u => u.UsuarioId == usuario.UsuarioId || u.Nome.Contains(usuario.Nome) || u.Cpf.Contains(usuario.Cpf) || u.Sexo.Contains(usuario.Sexo) || u.Fone.Contains(usuario.Fone) || u.DataCadastro == usuario.DataCadastro || u.CidadeId == usuario.CidadeId || u.Email.Contains(usuario.Email) || u.Senha == usuario.Senha);
            //System.Diagnostics.Debug.WriteLine(result.ToString());
            
            
            ViewBag.PaginaAtual = 1;
            return View(result.ToList());
        }

        [Authorize]
        public ActionResult IndexPagina(int pagina)
        {
            if (pagina < 0)
                pagina++;
            var usuarios = db.Usuarios.Include(u => u.Cidade).OrderBy(x => x.Nome).Skip((pagina-1)*10).Take(10);
            ViewBag.PaginaAtual = pagina;
            return View("Index", usuarios.ToList());
        }

        // GET: Usuario/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Usuario usuario = db.Usuarios.Find(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            return View(usuario);
        }

        // GET: Usuario/Create
        public ActionResult Create()
        {
            ViewBag.CidadeId = new SelectList(db.Cidades, "CidadeId", "Nome");
            return View();
        }

        // POST: Usuario/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UsuarioId,Nome,Cpf,Sexo,Fone,DataCadastro,CidadeId,Email,Senha")] Usuario usuario)
        {
            usuario.DataCadastro = DateTime.Today;
            
            if (ModelState.IsValid)
            {
                db.Usuarios.Add(usuario);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CidadeId = new SelectList(db.Cidades, "CidadeId", "Nome", usuario.CidadeId);
            return View(usuario);
        }

        // GET: Usuario/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Usuario usuario = db.Usuarios.Find(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            ViewBag.CidadeId = new SelectList(db.Cidades, "CidadeId", "Nome", usuario.CidadeId);
            return View(usuario);
        }

        // POST: Usuario/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UsuarioId,Nome,Cpf,Sexo,Fone,DataCadastro,CidadeId,Email,Senha")] Usuario usuario)
        {
            if (ModelState.IsValid)
            {
                db.Entry(usuario).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CidadeId = new SelectList(db.Cidades, "CidadeId", "Nome", usuario.CidadeId);
            return View(usuario);
        }

        // GET: Usuario/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Usuario usuario = db.Usuarios.Find(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            return View(usuario);
        }

        // POST: Usuario/Delete/5
        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Usuario usuario = db.Usuarios.Find(id);
            db.Usuarios.Remove(usuario);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Busca()
        {
            return View();
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
