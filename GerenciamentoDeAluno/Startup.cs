﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GerenciamentoDeAluno.Startup))]
namespace GerenciamentoDeAluno
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
