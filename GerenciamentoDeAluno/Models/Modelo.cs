﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace GerenciamentoDeAluno.Models
{
    public class Modelo{}

    public class Usuario {
        [Key]
        public int UsuarioId { get; set; }
        [Required]
        [StringLength(50)]
        public string Nome { get; set; }
        [Required]
        [StringLength(11, ErrorMessage="Tamanho do CPF muito extenso")]
        [MinLength(11, ErrorMessage="Tamanho do CPF muito curto")]
        [Display(Name = "CPF")]
        public string Cpf { get; set; }
        [Required(ErrorMessage = "Informe o sexo do usuário.")]
        public string Sexo { get; set; }
        [Required(ErrorMessage = "Informe o telefone do usuário.")]
        [Display(Name = "Telefone")]
        public string Fone { get; set; }
        [Display(AutoGenerateField = false)]
        public DateTime DataCadastro { get; set; }
        public Cidade Cidade { get; set; }
        public int CidadeId { get; set; }
        [Required(ErrorMessage = "Informe um email.")]
        [EmailAddress(ErrorMessage="Digite um email válido.")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Digite uma senha para o usuário.")]
        [DataType(DataType.Password)]
        public string Senha { get; set; }
        //public bool Lembrar { get; set; }
    }
    public class Aluno {
        [Key]
        public int AlunoId { get; set; }
        [Required(ErrorMessage = "Digite o nome do aluno.")]
        [MaxLength(50, ErrorMessage="Nome do aluno muito extenso.")]
        public string Nome { get; set; }
        [Required(ErrorMessage = "Digite o CPF.")]
        [StringLength(11)]
        [MinLength(11)]
        [Display(Name = "CPF")]
        public string Cpf { get; set; }
        [Required(ErrorMessage = "Informe o sexo do aluno.")]
        public string Sexo { get; set; }
        [Required(ErrorMessage = "Informe um telefone para contato.")]
        public string Fone { get; set; }
        [Display(AutoGenerateField = false)]
        public DateTime DataCadastro { get; set; }
        public Cidade Cidade { get; set; }
        public int CidadeId { get; set; }
        [Display(AutoGenerateField = false)]
        public DateTime DataUltimaModificacao { get; set; }
        public Usuario UsuarioModificador { get; set; }
        public int UsuarioModificadorId { get; set; }
        [MaxLength(50, ErrorMessage = "Nome do pai muito extenso.")]
        [Display(Name = "Nome do pai")]
        public string NomePai { get; set; }
        [Display(Name = "Numero RG do pai")]
        public string RgPai { get; set; }
        [Display(Name = "Telefone do pai")]
        public string FonePai { get; set; }
        [MaxLength(50, ErrorMessage = "A profissão do pai tem um nome muito extenso.")]
        [Display(Name = "Profissão do pai")]
        public string ProfissaoPai { get; set; }
        [MaxLength(50, ErrorMessage = "Nome da mãe muito extenso.")]
        [Display(Name = "Nome da mãe")]
        public string NomeMae { get; set; }
        [Display(Name = "Numero RG da mãe")]
        public string RgMae { get; set; }
        [Display(Name = "Telefone da mãe")]
        public string FoneMae { get; set; }
        [MaxLength(50, ErrorMessage = "A profissão da mãe tem um nome muito extenso.")]
        [Display(Name = "Profissão da mãe")]
        public string ProfissaoMae { get; set; }
    }
    public class Cidade {
        [Key]
        public int CidadeId { get; set; }
        [Required(ErrorMessage = "Digite o nome da cidade.")]
        [MaxLength(50,ErrorMessage="Diminua um pouco o tamanho do nome da cidade.")]
        public string Nome { get; set; }
        [Required(ErrorMessage = "Informe o estado.")]
        public string Estado { get; set; }
        [Required(ErrorMessage = "Digite o CEP.")]
        [Display(Name="CEP")]
        public string Cep { get; set; }
    }
}